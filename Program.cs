﻿using Newtonsoft.Json;
using System;
using System.Diagnostics;

namespace ReflectionHomework
{
    class Program
    {
        static void Main()
        {
            var person = new Person
            {
                FirstName = "Ivan",
                LastName = "Ivanov",
                Age = 25,
                Salary = 10000m,
                Married = true,
                Experience = 7
            };

            Stopwatch sw;
            int repeats = 100000;

            // Сериализация в наш формат...
            sw = Stopwatch.StartNew();
            for (int i = 0; i < repeats; i++)
            {
                MySerializer.SerializeObject(person);
            }
            sw.Stop();
            var serializedPerson = MySerializer.SerializeObject(person);
            Console.WriteLine($"Repeats: {repeats}, elapsed time: {sw.ElapsedMilliseconds} ms\nSerialized object:\n\t{serializedPerson}\n");

            // Десериализация из нашего формата...
            sw = Stopwatch.StartNew();
            for (int i = 0; i < repeats; i++)
            {
                MySerializer.DeserializeObject<Person>(serializedPerson);
            }
            sw.Stop();
            if (MySerializer.DeserializeObject<Person>(serializedPerson) is Person deserializedPerson)
                Console.WriteLine($"Repeats: {repeats}, elapsed time: {sw.ElapsedMilliseconds} ms\nDeserialized object:\n{deserializedPerson}\n");

            // Сериализация в JSON...
            sw = Stopwatch.StartNew();
            for (int i = 0; i < repeats; i++)
            {
                JsonConvert.SerializeObject(person);
            }
            sw.Stop();
            var jsonSerializedPerson = JsonConvert.SerializeObject(person);
            Console.WriteLine($"Repeats: {repeats}, elapsed time: {sw.ElapsedMilliseconds} ms\nSerialized object to JSON:\n\t{jsonSerializedPerson}\n");

            // Десериализация из JSON...
            sw = Stopwatch.StartNew();
            for (int i = 0; i < repeats; i++)
            {
                JsonConvert.DeserializeObject<Person>(jsonSerializedPerson);
            }
            sw.Stop();
            if (JsonConvert.DeserializeObject<Person>(jsonSerializedPerson) is Person jsonDeserializedPerson)
                Console.WriteLine($"Repeats: {repeats}, elapsed time: {sw.ElapsedMilliseconds} ms\nDeserialized object from JSON:\n{jsonDeserializedPerson}\n");
        }
    }
}
