﻿namespace ReflectionHomework
{
    class Person
    {
        public string FirstName = "";
        public string LastName = "";
        public int Age { get; set; } = 0;
        public decimal Salary { get; set; } = 0m;
        public bool Married { get; set; } = false;
        public int Experience { get; set; } = 0;

        public override string ToString()
        {
            return $"\tFirst Name: {FirstName}\n" +
                $"\tLast Name: {LastName}\n" +
                $"\tAge: {Age}\n" +
                $"\tSalary: {Salary}\n" +
                $"\tMarried: {Married}\n" +
                $"\tExperience: {Experience}\n";
        }
    }
}
