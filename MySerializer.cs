﻿using System;
using System.Linq;
using System.Reflection;
using System.Text;

namespace ReflectionHomework
{
    class MySerializer
    {
        public static string SerializeObject(object obj)
        {
            // Проверка на null
            if (!(obj?.GetType() is Type typeForSerialize))
                return "null";

            // Выбираем и объединяем в один список Поля и Свойства только примитивных типов, decimal и string
            // Другие сериализовывать не умеем
            var dataForSerialize = typeForSerialize.GetFields()
                .Select(x => new { x.Name, Type = x.FieldType, Value = x.GetValue(obj) })
                .Concat(typeForSerialize.GetProperties()
                    .Select(x => new { x.Name, Type = x.PropertyType, Value = x.GetValue(obj) })
                )
                .Where(x => x.Type.IsPrimitive || x.Type == typeof(decimal) || x.Type == typeof(string));

            // Генерируем сериализованную строку
            var sbSerialized = new StringBuilder();
            foreach (var elementForSerialize in dataForSerialize)
            {
                sbSerialized.Append("\"");
                sbSerialized.Append(elementForSerialize.Name);
                sbSerialized.Append("\":\"");
                sbSerialized.Append(elementForSerialize.Value);
                sbSerialized.Append("\",");
            }

            return sbSerialized.ToString().TrimEnd(',');
        }

        public static T DeserializeObject<T>(string str)
        {
            // Проверка на null
            if (str == "null")
                return default;

            var type = typeof(T);
            var obj = Activator.CreateInstance(type);

            // Разбиваем строку на пары "имя":"значение"
            foreach (var element in str.Trim('"').Split("\",\""))
            {
                var nameValuePair = element.Split("\":\"");

                // Количество элементов не 2 - переходим к следующей паре
                if (nameValuePair.Length != 2)
                {
                    continue;
                }

                // Это поле? Записываем значение
                if (type.GetField(nameValuePair[0]) is FieldInfo fieldInfo)
                {
                    var value = Convert.ChangeType(nameValuePair[1], fieldInfo.FieldType);
                    fieldInfo.SetValue(obj, value);
                }
                // Это свойство? Записываем значение
                else if (type.GetProperty(nameValuePair[0]) is PropertyInfo propertyInfo)
                {
                    var value = Convert.ChangeType(nameValuePair[1], propertyInfo.PropertyType);
                    propertyInfo.SetValue(obj, value);
                }
            }

            return (T)obj;
        }
    }
}
